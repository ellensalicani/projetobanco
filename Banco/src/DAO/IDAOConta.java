/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

/**
 *
 * @author ellen.salicani
 */
public interface IDAOConta<Tipo> {
    //ver como vai fazer pra passar o tipo da conta criada
    public void inserir(Tipo objeto) throws Exception;
    
    public void alterar(Tipo objeto) throws Exception;
    
    public void excluir(Tipo objeto) throws Exception;
    
    public void transfere(Tipo objeto) throws Exception;
    
    public void deposito(Tipo objeto) throws Exception;
    
    public void saque(Tipo objeto) throws Exception;
}
